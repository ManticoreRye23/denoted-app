<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    {{-- scripts --}}
     <script src="{{ asset('js/jquery.js') }}" defer></script>
     <script src="{{ asset('js/bootstrap.js') }}" defer></script>
    <style>
        .todo_done {
            text-decoration: line-through;
            color:red;
        }
    </style>
    <title>Document</title>
</head>
<body>
    <div class="container mt-5">
        @yield('content')
    </div>
</body>
</html>