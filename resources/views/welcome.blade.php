@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
   <div class="col-md-10">
      <div class="card">
         <div class="card-header text-center">Add Notes</div>
         <div class="card-body">
            <form action="{{route('note.store')}}" method="POST">
               @csrf
               <div class="form-group">
                  <label>Title</label>
                  <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{old('title')}}">
                  @error('title') 
                  <span class="invalid-feedback">
                  {{$message}}
                  </span>
                  @enderror
               </div>
               <div class="form-group">
                  <label>Text</label>
                  <textarea name="description" class="form-control @error('description') is-invalid @enderror">{{old('description')}}</textarea>
                  @error('description') 
                  <span class="invalid-feedback">
                  {{$message}}
                  </span>
                  @enderror
               </div>
               <div class="form-group">
                  <button class="btn btn-primary" type="submit">
                  Submit
                  </button>
               </div>
            </form>
         </div>
         <div class="card-footer"></div>
      </div>
   </div>
</div>
<div class="row justify-content-center">
   <div class="col-md-10">
      <div class="card">
         <div class="card-header text-center">All Notes</div>
         <div class="card-body">
            @if(count($notes) > 0)
            @foreach($notes as $note)
            <div class="card mb-2" style="width: 100%;">
               <div class="card-header">
                  <h4>{{$note->title}}</h4>
               </div>
               <div class="card-body">
                  <div class="row">
                     <div class="col-md-10">
                        <p class="card-text">{{$note->description}}</p>
                        <div class="row">
                           <div class="col-md-12">
                              <a class="card-link btn btn-success text-white" data-toggle="modal" data-target="#exampleModal{{$note->id}}">Edit</a>
                              <a class="card-link btn btn-danger text-white" data-toggle="modal" data-target="#deleteModal{{$note->id}}">Delete</a>
                              <a class="card-link btn btn-primary" data-toggle="collapse" href="#collapse{{$note->id}}" role="button" aria-expanded="false" aria-controls="collapse{{$note->id}}">
                              Add Todo
                              </a>
                           </div>
                        </div>
                            <div class="collapse" id="collapse{{$note->id}}">
                           <div class="card card-body mt-3 ">
                              <form action="{{route('todo.add',[$note->id])}}" method="POST">
                                 @csrf
                                 <div class="form-group">
                                    <label>Todo Name</label>
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}">
                                      @error('name')
                                    <span class="invalid-feedback">
                                       {{$message}}
                                    </span>
                                 @enderror
                                 
                                 </div>

                               
                                 <div class="form-group">
                                    <button class="btn btn-primary">Add</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                        
                        <hr>
                        <div class="row mt-3">
                           <div class="col-md-10">
                              <p class="font-weight-bold">Todo(s)</p>
                              <p>
                                 
                                 @foreach($note_todos as $note_todo)
                                 @if($note_todo->note_id == $note->id)
                                    <ul class="list list-unstyled">
                                       <li>
                                          <form action="{{route('todo.done',[$note_todo->id])}}" method="post">
                                             @csrf
                                             <input type="checkbox" 
                                                    name="todo_mark" {{ $note_todo->status == 1 ? "checked" : ""}}
                                                    onChange="this.form.submit()">&nbsp;
                                                     <a class="card-link btn btn-danger text-white float-right" data-toggle="modal" data-target="#deleteTodoModal{{$note_todo->id}}">Delete</a>

                                                      <a class="card-link btn btn-success text-white float-right mr-5" data-toggle="modal" data-target="#editTodoModal{{$note_todo->id}}">Edit</a>
                                          <span class="{{$note_todo->status == 1 ? "todo_done" : ""}}">{{$note_todo->name}}</span>
                                          </form>
                                       </li>
                                    </ul>
                              @endif
                               {{-- Edit Todo Modal --}}
                              <div class="modal fade" id="editTodoModal{{$note_todo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                 <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Update Todo</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                       </div>
                                       <div class="modal-body text-center">
                                            <form action="{{route('todo.update',[$note_todo->id])}}" method="POST">
                                          
                                             <input type="text" name="name_todo" value="{{$note_todo->name}}" class="form-control @error('name_todo') is-invalid @enderror">
                                             @error('name_todo')
                                                <span class="invalid-feedback ">
                                                   {{$message}}
                                                </span>
                                             @enderror
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        
                                             @csrf
                                             <button type="submit" class="btn btn-success">Update Todo</button>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              {{-- //Edit Todo Modal --}}

                               {{-- Delete Modal --}}
                                 <div class="modal fade" id="deleteTodoModal{{$note_todo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <h5 class="modal-title" id="exampleModalLabel">Delete Note</h5>
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                             </button>
                                          </div>
                                          <div class="modal-body text-center">
                                             Are you sure you want to delete this <b>todo?</b>
                                          </div>
                                          <div class="modal-footer">
                                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                             <form action="{{route('todo.destroy',[$note_todo->id])}}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Delete Todo</button>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 {{-- //Delete Modal --}}
                              @endforeach
                              </p>
                           </div>
                        </div>
                    
                     </div>
                  </div>
               </div>
            </div>
            {{-- Edit Modal --}}
            <div class="modal fade" id="exampleModal{{$note->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Note title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <form action="{{route('note.update',[$note->id])}}" method="POST">
                           @csrf
                           <div class="form-group">
                              <input type="text" name="edit_title" class="form-control @error('edit_title') is-invalid @enderror" value="{{$note->title}}">
                              @error('edit_title')
                              <span class="invalid-feedback">
                              {{$message}}
                              </span>
                              @enderror
                           </div>
                           <div class="form-group">
                              <textarea name="edit_description" class="form-control @error('edit_ description') is-invalid @enderror">{{$note->description}}</textarea>
                              @error('edit_description')
                              <span class="invalid-feedback">
                              {{$message}}
                              </span>
                              @enderror
                           </div>
                     </div>
                     <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-primary">Update Note</button>
                     </div>
                     </form>
                  </div>
               </div>
            </div>
            {{-- //Edit Note --}}
            {{-- Delete Modal --}}
            <div class="modal fade" id="deleteModal{{$note->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Note</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body text-center">
                        Are you sure you want to delete this <b>note?</b>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <form action="{{route('todo.destroy',[$note->id])}}" method="POST">
                           @csrf
                           <button type="submit" class="btn btn-danger">Delete Note</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            {{-- //Delete Modal --}}

            @endforeach
            @else
            <h2 class="text-center">{{'No note(s) to display'}}</h2>
            @endif
         </div>
      </div>
   </div>
</div>
@endsection