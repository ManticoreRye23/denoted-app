<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use Validator;

class ApiNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::all();
        return response()->json($notes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'title' => 'required',
            'description' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

       $note = new Note;
       $note->title = request('title');
       $note->description = request('description');
       $note->save();
       return response()->json($note);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)   
    {
        $note = Note::findOrFail($id);
        if(!$note) {
            return response()->json([
                'message' => 'Data Not Found'
            ],404);    
        }
        return response()->json($note);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        $note = Note::findOrFail($id);
          if(!$note) {
            return response()->json([
                'message' => 'Data Not Found'
            ],404);    
        }
        $note->title = request('title');
        $note->description = request('description');
        $note->save();
        return response()->json($note);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);
         if(!$note) {
            return response()->json([
                'message' => 'Data Not Found'
            ],404);    
        }
        $note->delete();

        return response()->json($note);
    }
}
