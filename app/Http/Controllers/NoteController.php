<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule; 
use App\Note;
use App\Todo;

class NoteController extends Controller
{
    public function store(Request $request) {
        $this->validate($request,[
            'title' => 'required|unique:notes,title',
            'description' => 'required'
        ]);

        Note::create([
            'title' => request('title'),
            'description' => request('description')
        ]);

        return redirect()->back();
    }

    public function index() {
        $notes = Note::orderBy('id','DESC')->paginate(10);
        $id = request('hidden_id');
        $note_todos = Todo::orderBy('id','DESC')->get();
        return view('welcome',compact('notes','note_todos'));
    }

    public function update($id, Request $request) {
        $this->validate($request,[
            'edit_title' => 'required|unique:notes,title,'.$id.'id',
            'edit_description' => 'required'
        ]);

        Note::where('id',$id)->update([
            'title' => request('edit_title'),
            'description' => request('edit_description')
        ]);

        return redirect()->back();
    }

    public function destroy($id) {
        Note::where('id',$id)->delete();

        return redirect()->back();
    }
}
