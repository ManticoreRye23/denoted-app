<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function addTodo($id,Request $request) {
        $this->validate($request,[
            'name' => 'required|unique:todos,name'
        ]);
        Todo::create([
            'note_id' => $id,
            'name' => request('name'),
            'status' => 0
        ]);

        return redirect()->back();
    }

    public function doneTodo($id) {
        $todo = Todo::findOrFail($id);
        $todo->status = !$todo->status; // Toggle if not equalt to 0 toggle 1 
        $todo->save();

        return redirect()->back();
    }

    public function updateTodo($id,Request $request) {
        $this->validate($request,[
            'name_todo' => 'required|unique:todos,name,'.$id.'id'
        ]);

        Todo::where('id',$id)->update([
            'name' => request('name_todo')
        ]);

        return redirect()->back();
    }

    public function destroy($id) {
        Todo::where('id',$id)->delete();

        return redirect()->back();
    }
}
