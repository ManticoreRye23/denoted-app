<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use Validator;

class ApiTodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::all();
        return response()->json($todos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules = [
             'note_id' => 'required|numeric',
             'name' => 'required',
             'status' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

       $todo = new Todo;
       $todo->note_id = request('note_id');
       $todo->name = request('name');
       $todo->status = request('status');
       $todo->save();
       return response()->json($todo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $todo = Todo::findOrFail($id);
        if(!$todo) {
            return response()->json([
                'message' => 'Data Not Found'
            ],404);    
        }
        return response()->json($todo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
             'note_id' => 'required|numeric',
             'name' => 'required',
             'status' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }
        
        $todo = Todo::findOrFail($id);
         if(!$todo) {
            return response()->json([
                'message' => 'Data Not Found'
            ],404);    
        }
        $todo->note_id = request('note_id');
        $todo->name = request('name');
        $todo->status = request('status');
        $todo->save();
    
        return response()->json($note);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $todo = Todo::findOrFail($id);
         if(!$todo) {
            return response()->json([
                'message' => 'Data Not Found'
            ],404);    
        }
        $todo->delete();

        return response()->json($todo);
    }
}
