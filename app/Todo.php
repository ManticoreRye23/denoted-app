<?php

namespace App;

use App\Note;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = ['note_id','name','status'];

}
