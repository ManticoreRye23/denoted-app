<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Todo`s
Route::post('/{id}/add','TodoController@addTodo')->name('todo.add');
Route::post('/{id}/done_todo','TodoController@doneTodo')->name('todo.done');
Route::post('/{id}/update_todo','TodoController@updateTodo')->name('todo.update');
Route::post('/{id}','TodoController@destroy')->name('todo.destroy');

Route::get('/','NoteController@index');
// Note`s
Route::post('/{id}/update','NoteController@update')->name('note.update');
Route::post('/{id}/delete','NoteController@destroy')->name('note.destroy');
Route::post('/','NoteController@store')->name('note.store');

