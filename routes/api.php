<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('notes','ApiNoteController');
Route::resource('todos','ApiTodoController');
// Route::post('/notes','ApiNoteController@store');
// Route::get('/notes','ApiNoteController@index');
// Route::get('/notes/{id}','ApiNoteController@show');
// Route::put('/notes/{id}','ApiNoteController@update');
// Route::delete('/notes/{id}','ApiNoteController@destroy');
